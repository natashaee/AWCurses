# The AWCurses Library
[![Build Status](https://travis-ci.org/TheGoldKnight23/AWCurses.svg?branch=master)](https://travis-ci.org/TheGoldKnight23/AWCurses)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![GitHub release](https://img.shields.io/github/tag/TheGoldKnight23/AWCurses.svg)](https://github.com/TheGoldKnight23/AWCurses/releases)


_A Wrapper for Curses - But C++_

THIS LIBRARY IS STILL UNDER DEVELOPMENT AND IS NOT READY TO BE USED


## Introduction

AWCurses is C++ wrapper and extension on the classic [curses] terminal
graphics library. It provides a powerful API and access to [curses]
where possible.

AWCurses _wraps_ [curses] by providing a C++ API, full of objects which
can be managed by smart pointers and play nice with C++11. Additionally
its API is properly object based, verses the C style functional/object
based API of [curses].

It _extends_ curses functionalities with several sublibraries (such as
[Menu]), and a [widget] system.

Additionally, since it wraps pure curses it can (and has) be compiled
for non-unix systems (aka windows), using the
[PDCurses] library.


## Design
General design and design goals of the library:

- Object orientated version of the curses library
- Abstracts a lot of the repetitive code needed to use curses (i.e
  creating a new WINDOW and storing its position for everything on the
  screen)
- Allows access and manipulation of underlying curses where possible
  without breaking or causing undefined behaviour
- Be error intolerable. (This library WILL throw _several_ errors at you
  if you try and set it up wrong or use it incorrectly (i.e Trying to
  write to a Screen before telling it to show() itself)).
- Provide an easy to use API without losing any of the power or
  functionality behind pure curses.



## Sub-Libraries

AWCurses is made up of a system of sub-libraries, each of which are mostly self-dependent (however they all depend on
[Core] and some on [Widgets]). The most basic version of AWCurses is the [Core] library, it provides all the backbone
functions and objects of AWCurses and is the pure wrapper part which has little additional (over [curses]) functionality
beyond its OOP API. All other sub-libraries of AWCurses build on the functionality of [AWCurses Core], expanding and/or
adding to it, usually to provide a more tailored API for a specific task (i.e making a [menu]).

Below is a list of the sub-libraries within AWCurses with links to their
relevant documentation pages.

- [Core]\: Provides the core of AWCurses and is required for its most basic use
- [Menu]\: Adds menus to AWCurses in a similar manner to the [curses] menu library (but does not wrap or use it)
- [Widgets]\: Provides an extensible widget system for AWCurses, comes with a few basic widgets
- [Form]\: Adds forms to AWCurses leveraging the [Widgets] library for full component swapping and extending (CONCEPT -
  IN EARLY DEVELOPMENT)


## Getting AWCurses

### Conan
Add the remote

`conan remote add TheGoldKnight23 https://api.bintray.com/conan/binary-frog-person/conan`

Then simply add 

`AWCurses/<VERSION_NUM>@TheGoldKnight23/develop` 

(The latest version as of writing is 0/12.0) to your `conanfile.txt` or `conanfile.py`.

#### Building from source

If you _really_ want to you _can_ compile from source. 

1. Download the latest stable release from github
2. Download and install the `meson` build system
3. Extract the archive somewhere
4. Make an out of source directory in which to build AWCurses in
5. Run `meson 'path/to/source/dir'` from your build directory
6. Run `ninja` in your build directory

You can also run the unit tests with `ninja test`

## License
This library is licensed under the [GNU GPLv3](/LICENSE)
>This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
>
>    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

<!-- Links -->
[Core]: /docs/Core/CORE.md
[AWCurses Core]: /docs/Core/CORE.md
[menu]: /docs/Menu/MENU.md
[widgets]: /docs/Widgets/WIDGETS.md
[widget]: /docs/Widgets/WIDGETS.md
[form]: /docs/Form/FORM.md

[pdcurses]: https://github.com/wmcbrine/PDCurses
[curses]: https://en.wikipedia.org/wiki/Curses_(programming_library)

