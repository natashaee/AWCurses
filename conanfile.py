from conans import ConanFile, Meson
from subprocess import run
from conans.tools import os_info

class AWCursesConan(ConanFile):
    name = "AWCurses"
    version = "0.12.0"
    license = "GPL-3.0-or-later"
    author = "Ash England-Elbro <ashenglandelbro@protonmail.com>"
    url = "https://github.com/TheGoldKnight23/AWCurses/"
    description = "An easy and powerful extension and wrapper library for curses"
    topics = 'c++', 'curses', 'ncurses', 'library', 'wrapper', 'conan', 'c++11'
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "pkg_config"
    no_copy_source = True
    exports_sources = "src/*", 'include/*', 'meson*', 'tests/*'
    user = 'TheGoldKnight23'
    channel = 'develop'
    build_requires = 'gtest/1.10.0'

   # def source(self):
    #    self.run("git clone https://github.com/TheGoldKnight23/AWCurses.git")
    def build(self):
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.test()

    def package(self):
        self.copy("*.hpp", keep_path=True)
        self.copy("*.inl", keep_path=True)
        self.copy("*awcurses*.dll", dst="bin", keep_path=False)
        self.copy("*awcurses*.so", dst="lib", keep_path=False)
        self.copy("*awcurses*.dylib", dst="lib", keep_path=False)
        self.copy("*awcurses*.a", dst="lib", keep_path=False)
        

    def requirements(self):
        if os_info.is_windows:
            # pdcurses on windows remote: alexwarrior-conan https://api.bintray.com/conan/alexwarrior-org/alexwarrior-conan
            self.requires('pdcurses/3.6@alex-precosky/stable')
        else:
            # ncurses on non-windows
            self.requires('ncurses/6.1@conan/stable')
            
    def package_info(self):
        base_libnames = ['libawcurses_core', 'libawcurses_menu', 'libawcurses_widgets']
        if self.settings.os == "Windows":
            for lib in base_libnames:
                self.cpp_info.libs.append(lib+".lib")
                if self.options.shared:
                    self.cpp_info.libs.append(lib+".dll")
        else:
            extension = ".so" if self.options.shared else ".a"
            for lib in base_libnames:
                self.cpp_info.libs.append(lib+extension)

