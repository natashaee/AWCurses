# The Screen class

> _Screen is the backbone class of AWCurses, as WINDOW is to pure curses._

## Overview
At the most basic level Screen directly wraps the curses WINDOW class and extends its
functionality (This is what it was initially designed to do).


It is designed to be more flexible than WINDOW, offering methods to display in different ways as well as simply not being displayed altogether. 


## How to use
Screen is relatively simple to use if you remember the following: **Nothing you do to the Screen will be displayed until you call Screen::show()**. So if you try and set the border a Screen object for example:

``` cpp
awcurses::core::Screen screen;
screen.setBorder('-', '|');
```

This will not take effect until _after_ `show()` is called (meaning the terminal will be empty).


This is important because some methods are designed to be blocking. For example `wait()` blocks until the user hits `ENTER` (or `RETURN` if you're American). So if I write the following code:

``` cpp
awcurses::core::Screen screen;
screen.wait();

std::cout << "After wait\n";

screen.show();

std::cout << "After show\n";

```

'After wait' will be printed immediately but 'After show' will only get printed after the user hits `ENTER`. This is because `screen.wait()` _doesn't actually get executed_ until `screen.show()` is called. 








