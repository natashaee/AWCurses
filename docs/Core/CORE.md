# AWCurses::Core Library Documentation


## Overview

This is the documentation page for the Core library within AWCurses.

The core library provides the core for AWCurses and is required for its
use.

## Namespace
The AWCurses::Core library uses the namespace: `awcurses::core`

## Provides
- Terminal - Provides an interface for managing the terminal session.
  The lifetime of the curses session is directly tied to the lifetime of
  the Terminal object.
- Screen - Provides methods extending and wrapping
  WINDOW. Used by any item which is displayed on the screen in any way.
- Cursor - Provides an interface for I/O with the terminal (reading, writing, getting input, etc)
- Callbacks.inl - A header file containing inline definitions of the basic AWCurses callback class
- Position - A type for an x and y coordinate vector within the terminal
- Size - a type for a height and width vector within the terminal
- Exceptions.h - A header file containing all the exceptions used by AWCurses::Core as well as AWCurses basic exception types
- KEYS.inl - A header file containing definitions for all the keytypes defined by ncurses and additional ones defined by AWCurses. The keys are stored within the `KEY` enum to allow static typing of keys.
