AWCurses::Widgets Library documentation
=======================================

Overview
--------
Widgets is a library which extends AWCurses functionality by adding
`Widget` items. These are items such as input fields for forms and
preset terminal environments like CmdInterface (which provides a
configurable terminal interface with commands).

The Widgets library is currently in development and, while its basic
functionality works, lacks many features and items.

Namespace
---------
The Widget library uses the namespace: `awcurses::widgets`.

Some widgets may extend this namespace with their own functions.

Provides
--------
- Widget - Base class which all widgets inherit from
- CmdInterface - Standard commandline environment manager with methods
  for adding commands.
- More to come


Extending functionality
-----------------------
This libraries functionality can be extended by creating custom widgets.
This can be done by inheriting from the Widget class or one of its
subclasses (for more specialised functionality)


