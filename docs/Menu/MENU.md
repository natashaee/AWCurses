AWCurses::Menu Library documentation
====================================

Overview
--------
The Menu library provides an extension to AWCurses functionality. It
adds the ability to create menus in the terminal.

These menus can be customised and created through the use of its `Menu`
class.

Namespace
---------
The menu library uses the namespace: `awcurses::menu`

Provides
--------
- Menu class
- MenuItem class

Extending functionality
-----------------------
The functionality of Menu can be extended most easily by having a class
which inherits from `MenuItem` and adding them to an existing menu via
`Menu::addItem()` which can take a `MenuItem` as its item and adds it to
the menu.


