var NAVTREE =
[
  [ "AWCurses", "index.html", [
    [ "The AWCurses Library", "index.html", null ],
    [ "AWCurses::Core Library Documentation", "md_docs__core__c_o_r_e.html", null ],
    [ "The Screen class<a name=\"Screen-Docs\"></a>", "md_docs__core__screen.html", null ],
    [ "The Terminal class", "md_docs__core__terminal.html", null ],
    [ "The Form Library", "md_docs__form__f_o_r_m.html", null ],
    [ "AWCurses::Menu Library documentation", "md_docs__menu__m_e_n_u.html", null ],
    [ "AWCurses::Widgets Library documentation", "md_docs__widgets__w_i_d_g_e_t_s.html", null ],
    [ "Bug List", "bug.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classawcurses_1_1core_1_1_screen.html#addbf3406f5f846b55e9ce2fce7b6e41f",
"classawcurses_1_1widgets_1_1_widget.html#a8cf8918ba7ec80b3f2d9b8b5a96bb303",
"group___curses.html#ga7818e96b553590d251fc81042ff3b475"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';