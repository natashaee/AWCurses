var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuwxyz~",
  1: "bcefgilmoprstw",
  2: "a",
  3: "abcdeghilmoprstuw~",
  4: "_acdefhikmnpstuwxyz",
  5: "cfkm",
  6: "cefklmw",
  7: "abt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Modules",
  7: "Pages"
};

