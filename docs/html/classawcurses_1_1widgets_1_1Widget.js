var classawcurses_1_1widgets_1_1Widget =
[
    [ "Widget", "classawcurses_1_1widgets_1_1Widget.html#ad533981dd94795b7d7412690df1ddbd8", null ],
    [ "Widget", "classawcurses_1_1widgets_1_1Widget.html#ae4dda5efbf9f33f3d533441f6b594b7f", null ],
    [ "~Widget", "classawcurses_1_1widgets_1_1Widget.html#afa5cb94472c47cef23790eab7947c3da", null ],
    [ "getHeight", "classawcurses_1_1widgets_1_1Widget.html#af062a44fb3d5b7fc677db0df3a1df4d8", null ],
    [ "getScreen", "classawcurses_1_1widgets_1_1Widget.html#aea989763a682e027102d88a79f467291", null ],
    [ "getWidth", "classawcurses_1_1widgets_1_1Widget.html#a17a9a1ceda65be673ea320ccd5393ede", null ],
    [ "getX", "classawcurses_1_1widgets_1_1Widget.html#a21bbd1aa32256d293653cf3c5e892a49", null ],
    [ "getY", "classawcurses_1_1widgets_1_1Widget.html#a1635087b1e9189c4b2049c1660d74dcb", null ],
    [ "setHeight", "classawcurses_1_1widgets_1_1Widget.html#aee8a8ac33978289ffae97ee1db997206", null ],
    [ "setScreen", "classawcurses_1_1widgets_1_1Widget.html#aaa7baf2e769dc4b297ef3797ff48f7bf", null ],
    [ "setWidth", "classawcurses_1_1widgets_1_1Widget.html#a250ee45d7d07c93b6bd8472998198e26", null ],
    [ "setX", "classawcurses_1_1widgets_1_1Widget.html#a8d8ddb4f7221def5fc626c1a5484920c", null ],
    [ "setY", "classawcurses_1_1widgets_1_1Widget.html#a9a0d426cef0352481d9a0eb8fac055de", null ],
    [ "show", "classawcurses_1_1widgets_1_1Widget.html#ac37c575104880fe19ae6d5665099c645", null ],
    [ "cursor", "classawcurses_1_1widgets_1_1Widget.html#aa592d4fd3933263632f063bbc166cabe", null ],
    [ "height", "classawcurses_1_1widgets_1_1Widget.html#ad3bed040e70ce4fe20d9a5ca1d2201da", null ],
    [ "parent", "classawcurses_1_1widgets_1_1Widget.html#abf2255db036ba0f2e3a438b96dcc13ea", null ],
    [ "screen", "classawcurses_1_1widgets_1_1Widget.html#ac12592ed45907bbe7e667d7eb3b303b0", null ],
    [ "width", "classawcurses_1_1widgets_1_1Widget.html#a66fbc65a4948f68e46831514d6b6179c", null ],
    [ "x", "classawcurses_1_1widgets_1_1Widget.html#a509c37adb558f101126199a2604e7706", null ],
    [ "y", "classawcurses_1_1widgets_1_1Widget.html#ac90c8cc1c3c8dd5432aeb66f14ad7993", null ]
];