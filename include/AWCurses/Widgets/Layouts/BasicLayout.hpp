/*/
Created by Ash on 11/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_BASICLAYOUT_HPP_08623DB1F64F4BC68BDE2B515D4D91C7
#define AWCURSES_BASICLAYOUT_HPP_08623DB1F64F4BC68BDE2B515D4D91C7


/**
 * @file Header file for BasicLayout
 */

#include <memory>
#include <vector>
#include <AWCurses/Core/Exceptions.hpp>
#include <AWCurses/Widgets/Widget.hpp>


namespace awcurses {
namespace core {
class Screen;
}
} // awcurses::core

namespace awcurses {
namespace widgets {

/**
 * @defgroup Layouts
 * @brief The layouts used by AWCurses Widgets
 */
/**
 * @addtogroup Layouts
 * @{
*/
namespace layouts {
class GenericBasicLayoutItem;
/// The base class for all widget layouts
/**
 * @class BasicLayout
 * @details This layout is used as the base class for all widget layouts
 */
class BasicLayout : public Widget {
public:
    /// Overloaded constructor. Takes its parent as its argument
    /**
     * @param parent: The parent to make the BasicLayout bind to
     */
    explicit BasicLayout(core::Screen *parent);
    
    /// Constructor which takes inital columns and rows explicitly
    /**
     * @param cols: Inital columns for the layout
     * @param rows: Inital rows for the layout
     */
    explicit BasicLayout(const core::Size& position = core::Size());
    
    /// Constructor which takes parent and inital starting dimensions
    /**
     * @param parent: Parent to bind to the layout
     * @param cols: Starting columns
     * @param rows: Starting rows
     */
    BasicLayout(core::Screen *parent, const core::Size& position = core::Size());
    
    /// Destructor
    ~BasicLayout() override = default;
    
    
    /// Add an item to the layout
    /**
     * Pure virtual, must be overrode by actual layouts
     * @param item: The item to add to the layout. Type will be set by inheriting layout
     */
    virtual void addItem(std::shared_ptr <GenericBasicLayoutItem> item);
    
    /// Selects the BasicLayout and causes it to capture the users cursor, taking all inputs entered
    /**
     * @throws exceptions::CursorError - View Screen::touch() for full possible throw list
     */
    virtual void select();

protected:
    /// Vector holding all the items in the layout
    std::vector <std::shared_ptr<GenericBasicLayoutItem>> items;
    
    /// Called by constructors to setup the Cursor object
    /**
     * By default sets the ESCAPE key to stop taking input, over layouts add to the list of custom keybinds through
     * this method
     */
    virtual void initCursor();
    
};

/**
 * @}
 */

}
}
} // awcurses::widgets::layouts


#endif //AWCURSES_BASICLAYOUT_HPP_08623DB1F64F4BC68BDE2B515D4D91C7
