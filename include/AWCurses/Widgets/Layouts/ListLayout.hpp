/*/
Created by Ash on 11/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_LISTLAYOUT_HPP_974BCC45698D4964BD40E8CD09DB9118
#define AWCURSES_LISTLAYOUT_HPP_974BCC45698D4964BD40E8CD09DB9118


/**
 * @file Header file for widgets ListLayout
 */

#include <AWCurses/Widgets/Layouts/BasicLayout.hpp>
#include <AWCurses/Widgets/Layouts/BasicLayoutItem.hpp>

namespace awcurses {
namespace widgets {
namespace layouts {

/// A layout which displays its items in a vertically stacked list
/**
 * @ingroup Widgets::Layouts
 */
class ListLayout : public BasicLayout {
public:
    // Inherit constructors
    using BasicLayout::BasicLayout;
    
    /// Add an item to the layout
    void addItem(std::shared_ptr <GenericBasicLayoutItem> item) override;
    
    /// Add an item of the specified type to the layout
    /**
     * This simply creates a smart pointer to the item of type T and passes it to ListLayout::addItem
     * (std::shared_ptr<GenericBasicLayoutItem>), it does however ensure that the item is the correct dimensions on
     * creation and is not displayed before initialisation
     * @tparam T: The type of item to create (i.e Widget), must is wrapped in a BasicLayoutItem, must fit its WT
     * template (BasicLayoutItem::WT)
     * @tparam ARGS: The template arguments to pass onto the new widget
     * @param args: The args to pass directly to the new widget's constructor
     * @warning The ARGS will be unpacked _before_ the x, y, width, height for the new item are passed
     */
    template <class T, typename ... ARGS>
    void addItem(ARGS &&...args);
    
    /// Move up an item in the layout
    virtual void upItem();
    
    /// Move down an item in the layout
    virtual void downItem();
    
    /// Overridden select method
    void select() override;
    
    /// Returns the item at the specific index
    /**
     * This will return a weak_ptr to the item at the specified index in the internal list of items. Wrapped by
     * operator[](int)
     * @param index: The index to return the value for
     * @return The item at the index specified
     * @throws std::out_of_range() if the item is out of range of the list of items
     */
    [[nodiscard]] std::weak_ptr <GenericBasicLayoutItem> itemAt(int index) const;
    
    /// Operator wrapper for itemAt(int)
    /**
     * @param index: The index to find the item at
     * @return The item at the specified index
     * @throws std::out_of_range() if the index is out of range
     */
    std::weak_ptr <GenericBasicLayoutItem> operator[](int index) const;

protected:
    /// The index of the currently selected item in the layout
    int current_index = 0;
    /// The size for each item in the layout
    core::Size item_size = core::Size(_screen.size().width() - 1, _screen.size().height() / 8);

    /// The position to place the next item in the layout
    core::Position next_position = _screen.position();

    void initCursor() override;
    
};


template <class T, typename ... ARGS>
void ListLayout::addItem(ARGS &&...args) {
    auto widget = std::make_unique<T>(args..., next_position, item_size);
    auto item   = std::make_shared<BasicLayoutItem<T>>(this, std::move(widget), next_position, item_size);
    addItem(item);
}

} // layouts
} // widgets
} // awcurses



#endif //AWCURSES_LISTLAYOUT_HPP_974BCC45698D4964BD40E8CD09DB9118
