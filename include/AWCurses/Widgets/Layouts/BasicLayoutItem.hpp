/*/
Created by Ash on 11/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_BASICLAYOUTITEM_INL_56F68EC592B64E8D854DD0EC5B0D1BCD
#define AWCURSES_BASICLAYOUTITEM_INL_56F68EC592B64E8D854DD0EC5B0D1BCD

/**
 * @file Header file for BasicLayoutItem
 */

#include <memory>
#include <AWCurses/Widgets/Widget.hpp>
#include <AWCurses/Widgets/Layouts/BasicLayout.hpp>


namespace awcurses {
namespace core {
class Screen;

} // awcurses::core
namespace widgets {
namespace layouts {
/// Generic base class for BasicLayoutItem
/**
 * @class GenericBasicLayoutItem
 * This generic class, which is inherited by BasicLayoutItem, allows storing of BasicLayoutItem (and decendents) in
 * statically typed containers (such as std::vector<>()).
 * @warning Make sure you only store **references** to objects of this type when using it for a wrapper class in
 * containers, otherwise you will get splicing
 */

class GenericBasicLayoutItem : public Widget {
public:
    // Inherit constructors
    using Widget::Widget;
    
    /// Interface select method
    virtual void select() = 0;
    
    /// Interface unselect method
    virtual void unselect() = 0;
};

/// Base class for layouts
/**
 * @class BasicLayoutItem
 * @details This class is used as a base for all layout item classes, which are used as items by their respective
 * layouts, which inherit from BasicLayout.
 * @ingroup Widgets::Layouts
 * @tparam WT: The widget item class which is wraps. Not a Widget (base widget class) due to Widget missing several
 * needed features. Recommended to inherit from Widget however as many of Widget's methods are required for this
 * template
 */
template <class WT>
class BasicLayoutItem : public GenericBasicLayoutItem {
public:
    /// Main constructor, builds a widget with specified dimensions
    /**
     * @param parent: The parent to bind to
     * @param x: The inital x of the widget
     * @param y: The inital y of the widget
     * @param width: The inital width of the widget
     * @param height: The inital height of the widget
     */
    explicit BasicLayoutItem(BasicLayout *parent, const core::Position& position = core::Position(), 
    const core::Size& size = core::Size()) : GenericBasicLayoutItem(&parent->screen(), position, size) {

    }
    
    /// Overloaded constructor, fills the parent layout
    /**
     * @param parent: The parent layout to bind to
     * @param widget: The widget which is contained in the BasicLayoutItem
     */
    BasicLayoutItem(BasicLayout *parent, std::unique_ptr <WT> widget) :
        BasicLayoutItem(parent, std::move(widget), parent->*_screen, 
        parent->*_screen) {}
    
    /// Constructor which takes parent and set values for x, y with and height
    /**
     * @param parent: The parent layout to bind to
     * @param widget: The widget to contain in the BasicLayoutItem
     * @param x: Inital x of the widget item
     * @param y: Inital y of the widget item
     * @param width: Inital width of the widget item
     * @param height: Inital height of the widget item
     */
    BasicLayoutItem(BasicLayout *parent, std::unique_ptr <WT> widget, const core::Position& position = core::Position(), 
    const core::Size& size = core::Size()) : GenericBasicLayoutItem(&parent->screen(), position, size), widget(std::move(widget)){}
    
    /// Sets the layout item's widget
    /**
     * @param set_widget: A pointer to the widget to set, takes ownership with std::move()
     */
    virtual void setWidget(std::unique_ptr <WT> set_widget);
    
    /// Get the widget wrapped by the BasicLayoutItem
    /**
     * @return A raw pointer to the widget wrapped by the BasicLayoutItem (of template type WT)
     */
    [[nodiscard]] virtual WT *getWidget() const;
    
    /// Selects the widget item contained in the BasicLayoutItem
    void select() override;
    
    /// Unselect the widget item contained in the BasicLayoutItem
    void unselect() override;

    /// The widget which this layout item contains/wraps
    std::unique_ptr <WT> widget;
};

template <typename WT>
void BasicLayoutItem<WT>::select() {
    widget->select();
}

template <typename WT>
void BasicLayoutItem<WT>::unselect() {
    widget->unselect();
}

} // layouts
} // widgets
} // awcurses

#endif //AWCURSES_BASICLAYOUTITEM_INL_56F68EC592B64E8D854DD0EC5B0D1BCD
