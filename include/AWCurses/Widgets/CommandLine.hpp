/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 30/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_COMMANDLINE_HPP
#define AWCURSES_COMMANDLINE_HPP

#include <string>
#include <functional>
#include <map>
#include <AWCurses/Core/Callback.inl>
#include <AWCurses/Widgets/Widget.hpp>

/**
 * @file Header file for Commandline widget
 */

namespace awcurses {
namespace core {
class Screen;

class Cursor;
}
}

namespace awcurses {
namespace widgets {

/**
 * @class CommandLine
 * @brief Widget which provides a standard commandline interface
 * @details Provides a standard commandline interface with an API allowing customisation of commands and callbacks.
 * @ingroup Widgets
 */
class CommandLine : public Widget {
public:
    class CommandCallback : public core::callbacks::ExeCallback {
    public:        
        CommandCallback(func_pattern func, std::string command, std::string
        help_txt, bool use_regex = false);
        
        /// The command to match with, can be a regex if use_regex is set to true
        std::string command;
        /// Text displayed to inform the user about the command
        std::string help_txt;
        /**
         * @brief Whether to use regex or not to match commands (i.e .*name?. would match anything with 'name' in it as
         * the command).
         * @warning This uses the C++ standard library `std::regex` which has several performance problems, if performance is an issue
         * it is recommended to not use this feature and not set this variable to `true`
         */
        bool        use_regex = false;
    };
    
    /// The callback function definition
    /**
     * This is the definition for the callback function used for CommandLine callbacks. The string passed is the full
     * message (including the command) which the user typed. This is intended to allow parsing for additional
     * arguments.
     *
     * Commands can be added without having to take const std::string& by calling the alternative of version of
     * addCommand()
     */
    using command_callback = CommandCallback;
    
    /**
     * @brief Constructor which sets the startup message
     * @param startup_msg
     */
    explicit CommandLine(std::string startup_msg = "");
    
    /**
     * @brief Constructor which sets the startup message AND the screen
     * @param screen
     * @param startup_msg
     */
    explicit CommandLine(core::Screen *screen, std::string startup_msg = "");
    
    
    /// Default destructor
    ~CommandLine() override;
    
    
    /// Allows adding a custom keybind without a returned text
    /**
     * This method wraps Cursor::addKeybind(int, keybind_callback) to provide a way to add callbacks which do not
     * need to receive the command which they are called on. Since it takes a raw pointer it is responsible for the
     * object's deconstruction
     * @param key: The key to set
     * @param callback: The callback to call when the key is pressed
     */
    void addCommand(const command_callback& callback);
    
    // Methods
    /**
     * @brief Starts the commandline running and prints welcome msg
     */
    void start();
    
    /**
     * @brief Prints the startup message
     */
    void printStartupMsg();
    
    /// Print out the help messages for all commands which have one
    void printHelp();
    
    /// Prints to the terminal in a safe manner
    /**
     * @details Prints the terminal while managing the Cursor object to ensure no weirdness occurs. Because of
     * the complex nature of handling the Cursors position on the screen in the managed environment of the
     * commandline widget, you should *not* attempt to print to the screen manually using the widgets Cursor. Use
     * this method instead
     * @param msg: The message to print
     * @param move_lines: Optional parameter to move in the y direction, defaults to 1 for a newline. If set to 0
     * the Cursors position will not reset
     * @throws std::logic_error() if move_lines < 0. (this is to prevent undefined behaviour otherwise)
     * @bug Currently printing is a little broken. It _may_ work or it _may_ print halfway across the screen.
     */
    void print(const std::string &msg, int move_lines = 1);
    
    /// Getter for the internal commands used by CommandLine
    const std::vector <command_callback> &commands();
    
    /// The startup message used by the widget
    std::string startup_msg = "A commandline interface\n";
private:
    
    /// Commands used internally by the widget
    std::vector <command_callback> _commands;
    
    /**
     * @brief Adds the default commands to the list of commands. Called in constructors
     * @throws std::runtime_error if its list of commands is empty (wont be unless cleared on purpose)
     */
    void addDefaultCommands();
};
}
}

#endif //AWCURSES_COMMANDLINE_HPP
