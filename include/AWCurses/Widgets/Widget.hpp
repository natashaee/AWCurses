/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 14/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_WIDGET_HPP
#define AWCURSES_WIDGET_HPP

#include <memory>
#include <curses.h>
#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/Position.hpp>
#include <AWCurses/Core/Size.hpp>


/**
 * @file Header file for Widgets class
 */

/**
 * @defgroup Widgets
 * @brief Widgets library which extends AWCurses functionality with widgets
 * @details Provides a library of 'widgets' which extends AWCurses functionality in multiple ways
 */

/**
 * @namespace awcurses::widgets
 * @brief Contains the Widget extension library for AWCurses
 * @ingroup Widgets
 */

namespace awcurses {
namespace widgets {
/**
 * @class Widget
 * @brief Base class for widgets
 * @details Base class which _all_ widgets in AWCurses inherit from
 * @ingroup Widgets
 */
class Widget {
public:
    // Constructors and deconstructions
    
    
    /// Constructs Widget with x, y, width, and height values
    /**
     * @param position: Inital position for the Widget
     * @param size: Initial size for the Widget
     */
    explicit Widget(const core::Position& position = core::Position(), const core::Size& size = core::Size());
    
    /// Constructs the Widget with a screen parent and passed x, y, width and height values
    /**
     * @param parent: The Widget's parent Screen which it will display inside
     * @param position: Inital position for the Widget
     * @param size: Initial size for the Widget
     */
    
    explicit Widget(core::Screen *parent, const core::Position& position = core::Position(), const core::Size& size = core::Size());
    
    
    /// The default destructor for the widget base class
    virtual ~Widget();
    
    
    /// Displays the Widget by calling it's Screen's Screen::show() method
    virtual void show();
    
    /// The Screen object used by the Widget
    const core::Screen& screen() const { return _screen; }
    /// Non-const getter for the Screen object used by the Widget
    core::Screen& screen() { return _screen; }

    /// The Position of the Widget
    const core::Position& position() const { return _position; }
    /// The Size of the Widget
    const core::Size& size() const { return _size; }

    /// Non-const accessor for size
    core::Size& size() { return _size; }

    /// Non-const accessor for position
    core::Position& position() { return _position; }

    /// Set the Position of the Widget
    void position(const core::Position& position) { _position = position; }

    /// Set the Size of the Widget
    void size(const core::Size& size) { _size = size; }

protected:
    core::Screen _screen;

    core::Screen *parent = nullptr;

    core::Position _position;

    core::Size _size;
};
} // widgets
} //awcurses


#endif //AWCURSES_WIDGET_HPP
