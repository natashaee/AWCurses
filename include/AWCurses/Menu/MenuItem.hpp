/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 16/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_MENUITEM_HPP
#define AWCURSES_MENUITEM_HPP


#include <curses.h>
#include <string>
#include <functional>
#include <memory>
#include <AWCurses/Widgets/Widget.hpp>
#include <AWCurses/Core/Screen.hpp>

/**
 * @file Header file for the MenuItem class
 */


namespace awcurses {
namespace core {
class Cursor;
}

namespace menu {
class Menu;

/**
 * @class MenuItem
 * @brief An item for a Menu
 * @ingroup Menu
 */

class MenuItem : public widgets::Widget {
public:
    /// Callback definition for MenuItem's
    using menu_callback = std::function<void()>;
    // Constructors and destructors
    /**
     * @brief Constructor for Menu item
     * @details Constructor for Menu item which takes initial x, y, width and height for the item
     * @param menu: Menu to be bound to
     * @param x: Initial x
     * @param y: Initial y
     * @param width: Item width
     * @param height: Item height
     * @param selectable: Whether the item should be selectable or not
     */
    
    MenuItem(Menu *menu, const core::Position& position, const core::Size& size, bool selectable = false);
    
    /**
     * @brief Constructor for Menu item
     * @details Constructor for Menu item which takes initial x, y, width and height for the item. Additionally takes
     * a description and title for initial values.
     * @param menu: Menu to be bound to
     * @param title: Title to be set for the item
     * @param description: Description to be set for the item
     * @param x: Initial x
     * @param y: Initial y
     * @param width: Item width
     * @param height: Item height
     * @param selectable: Whether the item should be selectable or not
     */
    
    MenuItem(Menu *menu, std::string title, std::string description, const core::Position& position, const core::Size& size, bool selectable = false);
    
    // Menu methods
    /**
     * @brief Select the item
     * @return Error code
     */
    void select();
    
    /**
     * @brief Unselect the item
     * @return Error code
     */
    void unselect();
    
    /**
     * @brief Show the item (called on initialisation)
     * @throws exceptions::InvalidScreenError
     */
    void show() override;
    
    /**
     * @brief Hide the item
     * @throws exceptions::InvalidScreenError
     */
    void hide();
    
    /**
     * @brief Trigger the items callback if one is set
     */
    void triggerCallback();
    
    // Curses
    /**
     * @brief Redraw the item
     */
    void redraw();
    
    // Variables
    
    /// The title of the MenuItem
    std::string   title       = "TITLE";
    /// The description of the MenuItem
    std::string   description = "DESCRIPTION";
    /// Whether the MenuItem is selectable (currently broken)
    bool          selectable  = false;
    /// The callback triggered when the MenuItem is selected or used in some way
    menu_callback callback;
protected:
    /// The parent reference to Menu for the MenuItem
    Menu *menu;
    /// Whether the MenuItem is selected
    bool selected             = false;
    
    
};

}
} // awcurses::menu


#endif //AWCURSES_MENUITEM_HPP

