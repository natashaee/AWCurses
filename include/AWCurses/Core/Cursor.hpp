/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 05/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_CURSOR_H
#define AWCURSES_CURSOR_H

#include <string>
#include <map>
#include <functional>
#include <AWCurses/Core/Callback.inl>
#include <AWCurses/Core/KEYS.inl>
#include <AWCurses/Core/Position.hpp>

/**
 * @file Header file for the Cursor class
 */

namespace awcurses {
namespace core {

class Screen;

/**
 * @class Cursor
 * @brief Controls the IO of a Screen
 * @ingroup Core
 */
class Cursor final {
public:
    /// The callback class for Cursor custom keybindings
    /**
     * @class CursorCallback
     * @brief Callback object for AWCurses Cursor
     * @attention This callback's return value is evaluated to decide whether to keep taking input or not. Read the
     * documentation on this ***before*** using it
     * @details This defines the function structure required for Cursor's custom keybind callbacks. The boolean
     * return is evaluated to check whether or not to stop taking input and return. If  this function returns _true_
     * then it will act as an end of input key (i.e ENTER) and will cause the Cursor
     * object to break out of its input loop. If it returns _false_ then the Cursor will continue taking input
     * @returns true: Break out of the input loop after executing, false: Continue taking input
     */
    class CursorCallback : callbacks::Callback<bool, int> {
    public:
        /// Constructor. Intentionally allows implicit construction from function objects
        CursorCallback(std::function<bool(int)> func, bool canceling = false);
        
        /// Overriden and defined call operator for CursorCallback
        bool operator()(int key) override;
        
        /// The value returned when called with the call operator
        /**
         * This value specifies whether to stop taking user input after calling
         */
        bool canceling = false;
    };
    
    /// Callback alias definition
    using keybind_callback = CursorCallback;
    
    
    /**
     * @brief Overloaded - coordinate specifying constructor for Cursor
     * @param screen: Screen pointer of some kind to bind to
     * @param x: X coordinate to start at and return to
     * @param y: Y coordinate to start at and return to
     */
    Cursor(Screen *screen, const Position& position = Position());
    
    /// Default copy constructor
    Cursor(Cursor& c) = default;
    
    /// Move constructor/// Default copy constructor, protected to prevent slicing
    Cursor(const Cursor &old) = default;
    Cursor(Cursor &&c) noexcept;
    
    /// Default destructor
    ~Cursor() = default;
    
    /// Set the Screen object that the Cursor is bound too
    void set_parent(Screen *set);
    
    // Zero the location
    /**
     * @brief Reset the cursor back to it initial coordinates
     */
    void resetPos();
    
    /// Move in the x direction
    /**
     * @param move: Amount to move in the x direction
     * @throws exceptions::OutOfBoundsException if the new x coordinate is smaller than 0 (off the screen)
     */
    void moveX(int move);
    
    /// Move in the y direction
    /**
     *
     * @param move: Amount to move in the y direction
     * @throws exceptions::OutOfBoundsException if the new y coordinate is smaller than 0 (off the screen)
     */
    void moveY(int move);
    
    /// Set the current x coordinate as its reset x
    void lockX();
    
    /// Set the current y coordinate as its reset y
    void lockY();
    
    /// Reset the x coordinate
    void resetX();
    
    /// Reset the y coordinate
    void resetY();
    // General functions
    /**
     * @brief Gets input from the bound Screen.
     * @details Wraps Cursor::getInput(std::stringstream &buffer,  size_t max_len
     * = -1, char mask = ' ') with a handy interface for basic types
     * @param buffer: The buffer to read the users input into
     * @param max_len: The max lenght of the buffer. Should be 1 less than the size of the buffer to account for the \0
     * @param mask: The mask to apply to the input. Set to ' ' (blank) to echo input
     * @return The number of characters entered
     * @throws Rethrows any exceptions from Cursor::getInput(std::stringstream)
     */
    size_t getInput(char *buffer, size_t max_len, char mask = ' ');
    
    /// Get input from the bound Screen using a stringstream as a buffer
    /**
     * @brief Gets input from the bound Screen
     * @param buffer: The buffer to read the users input into
     * @param max_len: The max lenght of the users input. Defaults to -1 which disables this setting
     * @param mask: The mask to apply to the input. Set to ' ' (blank) to echo input
     * @return The number of characters entered
     * @throws exceptions::InvalidScreenError
     * @throws exceptions::Exception when its Screen reference is a (dangling) nullptr reference
     */
    size_t getInput(std::stringstream &buffer, size_t max_len = -1, char mask = ' ');
    
    /// Capture input from the user
    /**
     * This will assume all inputs are commands, unlike getInput() which will try and process them as entered keys
     * Will not echo to the terminal even if Cursor::echo is set to true (which it is by default)
     * @throws exceptions::InvalidScreenError
     * @throws exceptions::Exception when its Screen reference is a (dangling) nullptr reference
     */
    void captureInput();
    
    /// Get a single input from the user
    /**
     * @param echo: Whether to echo input to the terminal
     * @throws exceptions::InvalidScreenError
     * @return The integer (ASCII or UTF8) value of the entered character
     */
    keys::char_c getchar(bool echo);
    
    /// Get a single input from the user with default echo mode
    /**
     * Wraps Cursor::getch(bool), passing the current value of Cursor::echo to it
     * @return The integer (ASCII or UTF8) value of the entered character
     * @throws exceptions::InvalidScreenError
     */
    keys::char_c getchar();
    
    /**
     * @brief Print a line to the Screen then newline
     * @param msg: The message to print to the Screen
     * @param search_newlines: Boolean value of whether to search the string for '\\n' (newlines) and move in the y
     * direction to account. Disabled by default as searching the message takes O(n) time
     * @throws Same as Cursor::print()
     */
    void println(const std::string &msg, bool search_newlines = false);
    
    /**
     * @brief Prints a message and moves specifed distance
     * @param msg: The message to print
     * @param move_x: The additional distance to move after printing in the x (horizontal) direction
     * @param move_y: The additional distance to move after printing in the y (vertical) direction
     * @param search_newlines: Boolean value of whether to search the string for '\\n' (newlines) and move in the y
     * direction to account. Disabled by default as searching the message takes O(n) time
     * @throws exceptions::InvalidScreenError() if its Screen has not been setup
     * @throws exceptions::Exception() if its Screen is a nullptr
     */
    void print(const std::string &msg, int move_x = 0, int move_y = 0, bool search_newlines = false);
    
    /// Print a single character
    /**
     * @param msg: The message to print
     */
    void print(char msg);
    
    /**
     * @brief Erases the numbers of characters printed up to num or all if num > all
     * @param num: The maximum number to erase
     * @throws exceptions::InvalidScreenError()
     */
    void erase(int num = 1);
    
    /**
      * @brief Erases all the items typed since last call to lockPosition();
     */
    void eraseAll();
    
    /**
     * @brief Set the zeroX and zeroY values to the current position of the cursor.
     * @details Set the cursor to return to its current position whenever resetPos() is called or it resets
     * itself. Also resets the counter on how many characters have been typed
     */
    void lockPosition();
    
    /// Adds a custom key with the set callback function
    /**
     * @details Adds the key passed to the list of custom keybindings which are checked before passing the input to
     * the hardcoded bindings (such as ENTER or SPACE)
     * @param key: The keypress to register with. This is checked directly against the key captured by curses
     * @param callback: The function to call when the key is pressed. Must be of type Cursor::keybind_callback
     */
    void addKeybind(int key, keybind_callback callback);
    
    // Operators
    /// Print operator wrapper
    /**
     * @param msg: message to print
     * @throws Same as the normal Cursor::print()
     */
    void operator<<(const std::string &msg);


    /// getInput(std::stringstream) operator wrapper
    /**
     * @param buffer: The buffer to read input into
     * @throws Rethrows/doesnt catch anything from Cursor::getInput(std::stringstream)
     */
    int operator>>(std::stringstream &buffer); // Wrapper for getInput
    
    /// Move assignment operator
    Cursor &operator=(Cursor &&c) noexcept;
    
    /// Copy assignment operator
    Cursor &operator=(const Cursor& c) = default;
    
    /// Getter for the parent of the Cursor
    [[nodiscard]] const Screen *parent() const;

    /// Whether to echo characters to the terminal
    bool echo() const { return _echo; }

    /// Setter for echo
    void echo(bool echo = true) { _echo = echo; }
   
    
    /// The position of the current Cursor object
    const Position& position() const { return _position; }

    /// The reset Position of the Cursor object
    const Position& zero_position() const { return _zero_pos; }

    /// Non-const accessor for the position
    Position& position() { return _position; }
    /// Non-const accessor for zero_position
    Position& zero_position() { return _zero_pos; }

    /// Set the current cursor position
    void position(const Position& position) { _position = position; }

    /// Set the Cursor object's reset position
    void zero_position(const Position& zero_position) { _zero_pos = zero_position; }
private:

    /// Stores the current position of the Cursor object
    Position _position;
    
    /// Stores the zero position (reset position) of the Cursor
    Position _zero_pos;

    /// The total amount the Cursor has moved
    int                             moved_total = 0;
    /// The parent screen object
    Screen                          *_parent;
    /// Map of custom keybindings to capture
    std::map<int, keybind_callback> _custom_bindings;


    /// Boolean of whether using custom keybindings to avoid unnecessary lookups
    bool _using_custom_bindings = false,
    /// Whether to echo characters to the terminal
         _echo                  = true; 
}; 

} // core
} // awcurses


#endif //AWCURSES_CURSOR_H
