/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 05/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_SCREEN_H
#define AWCURSES_SCREEN_H

#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Position.hpp>
#include <AWCurses/Core/Size.hpp>
#include <vector>
#include <string>
#include <memory>
#include <curses.h>
#include <queue>

/**
 * @file Header file for the Screen class
 */

namespace awcurses {
namespace core {

/**
 * @class Screen
 * @brief Extends and wraps curses WINDOW
 * @details Screen wraps and extends the curses WINDOW class. It can be used either like a WINDOW object for other
 * AWCurses classes, or as a full on screen manager representing a single 'screen' in the users terminal.
 * More details can be found in Screen.md
 * @ingroup Core
 */
class Screen final {
public:

    /**
     * @brief Default constructor for Screen
     * @param x: The x coordinate for the Screen to start from
     * @param y: The y coordinate for the Screen to start from
     * @param height: The height of the new Screen
     * @param width: The width of the new Screen
     */
    explicit Screen(const Position& position = Position(), const Size& size = Size());
    
    /// Constructor which initialises the Screen with a Cursor
    explicit Screen(Cursor cursor, const Position& position = Position(), const Size& size = Size());
    
    
    /**
     * @brief Default constructor for Screen
     * @details Deconstructs the Screen object and deletes all its associated pointers. (This includes all cursors)
     */
    ~Screen() = default;
    
    /// Get whether the Screen object has been setup yet
    /**
     * Enables checking of whether the Screen object has yet been setup, outside of trying to call a method which
     * will throw otherwise and catching it.
     * @returns Boolean value of whether the Screen is setup yet
     * 
     * - true: It is and you can safely call functions which require it to be setup
     * - false: Its not and you better set it up or have some trycatch loops ready
     */
    [[nodiscard]] bool is_setup() const;
    
    
    /// Shows and sets up the Screen
    /**
     * @details The init method for Screen. Due to limitations of the curses library it is impossible to create a
     * WINDOW object without it immediately displaying itself (as far as I know). Having
     * a new window displaying  _every time_ a new Screen is created would drastically reduce Screen's flexibility by
     * tying it very closely to curses and making it more of a resource manager for WINDOW, which is not its design purpose.
     * Therfore, this method exists - allowing Screen to be used more philosophically as a representative object.
     *
     * The tradeoff is that you must call this method before any changes you make to the Screen (like changing its colour)
     * are displayed. For more details please see the SCREEN.md file under /docs
     * @warning When this method is called, every method in the Screen object's instruction queue is executed
     */
    void show();
    
    /**
     * @brief Hides the Screen
     */
    void hide();
    
    /**
     * @brief Holds the terminal open and waits for user input
     * @return Error code
     * @throws exceptions::InvalidScreenError
     */
    void wait();
    
    /// Clears the Screen
    /**
     * @throws exceptions::InvalidScreenError if it doesnt have a window associated with it when called
     */
    void clear();
    
    /**
     * @brief Refreshes the Screen
     * @throws exceptions::InvalidScreenError()
     */
    void refresh();
    
    // Colour
    
    ///Sets the colour of the window
    /**
     * @param color_num: The id of the colour pair to set it as
     * @throws exceptions::InvalidScreenError if the Screen object has not been setup yet
     */
    void setColour(int color_num);
    
    /**
     * @brief Sets the colour as default for the Terminal
     */
    void setColourDefault();
    
    /**
     * @brief Sets the selected colour from the Terminal
     */
    void setColourSelected();
    
    // Border
    /**
     * @brief Sets the border
     * @param top: Top char
     * @param right: right char
     * @param top_right: top right char
     * @param bottom_right: bottom right char
     * @param left: left char
     * @param top_left: top left char
     * @param bottom_left: bottom left char
     * @param bottom: bottom char
     */
    void
    setBorder(char top, char right, char top_right, char bottom_right, char left, char top_left, char bottom_left,
              char bottom);
    
    /**
     * @brief Sets the border
     * @param left_right: Chars for the left and right of the window
     * @param top_bottom: Chars for the top and bottom of the window
     */
    void setBorder(char left_right, char top_bottom);
    
    /**
     * @brief Sets to the default border
     */
    void setLineBorder();
    
    /**
     * @brief Removes the Screen's border
     */
    void clearBorder();
    
    /// 'Touches' the underlying curses WINDOW
    /**
     * Sometimes useful to force the Screen to be displayed on top of anything else and capture user input
     * @throws exceptions::InvalidScreenError if the Screen hasnt been set up with Screen::show()
     */
    void touch();
    
    /// Bool operator, returns the value of Screen::is_setup()
    explicit operator bool();
    
    /// Const throwing getter for the WINDOW* object used by Screen
    [[nodiscard]] WINDOW* curses_window() const;

    /// Getter for the WINDOW* object used by Screen which automatically calls show() if not setup
    [[nodiscard]] WINDOW* curses_window();
    
    /// Creates a managed window with C style curses deleted out of passed raw WINDOW pointer
    static std::unique_ptr<WINDOW, std::function<void(WINDOW*)>> makeManagedWindow(WINDOW* win);

    /// The Cursor object bound to the Screen
    const Cursor& cusor() { return _cursor; }

    /// Non-const version of the cursor getter
    Cursor& cursor() { return _cursor; }

    /// Set the Screen obeject's Cursor
    void cursor(const Cursor& cursor) { _cursor = cursor; }

    /// The Screen object's Position
    const Position& position() const { return _position; }

    /// Non-const accessor for the position
    Position& position() { return _position; }

    /// The Screen object's Size
    const Size& size() const { return _size; }

    /// Non-const accessor for size 
    Size& size() { return _size; }

    /// Set the Screen obeject's Position
    void position(const Position& position) { _position = position; }
    /// Set the Screen object's Size
    void size(const Size& size) { _size = size; }

private:

    Cursor _cursor;

    Position _position;

    Size _size;

    bool   setup = false;
    std::unique_ptr<WINDOW, std::function<void(WINDOW*)>>
    /// WINDOW used internally by the Screen object
    _win,
    /// WINDOW used to wrap the Screen objects internal WINDOW
    top_win;

    /// The type for an 'instruction' object
    using instruction_t = std::function<void(void)>;

    /// The queue of functions waiting to be executed by the Screen object before show() is called
    std::queue<instruction_t> _instruction_queue;

    /// Add an instruction to the queue
    /**
     * If the Screen object has already been shown then the instruction is executed immeditatly. Else
     * it is added to the queue to be executed by show()
     */
    void addInstruction(instruction_t instruction);
    
};
} // core
} // awcurses

#endif //AWCURSES_SCREEN_H

