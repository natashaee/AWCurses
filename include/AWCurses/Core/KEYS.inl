
/*/
Created by Ash on 11/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_KEYS_INL_05E3204E9B2242BB88D637BFC589C61F
#define AWCURSES_KEYS_INL_05E3204E9B2242BB88D637BFC589C61F

/**
 * @file KEYS file contains extended definitions of ascii keys
 */

/**
 * @defgroup Keys
 * @brief Integer values for key presses
 * @attention Credits for the standard curses keys go to the PDCurses devs 
 * @details Contains both the curses key set (with constexpr auto instead of #define) and an extended set of keys
 * which are all ASCII at this time 
 */

/**
 * @addtogroup Keys
 * @{
 */

/**
 * @namespace awcurses::core::keys
 * @brief Namespace containing all the integer key values of inputs for both curses and AWCurses
 */

namespace awcurses {
namespace keys {

/// Definition of char_c, the basic type used for characters
/**
 * wchar_t is used as characters are dealt with as ASCII/Unicode sequences
 */
using char_c = wchar_t;

/**
 * @enum KEY
 * @brief enum type for a keycode
 * @details Provides a type for keycodes allowing strict typing rules to be used on keycodes - 
 * something not possible with curses/ncurses/pdcurses `#define`s
 */
enum KEY: char_c {

/// Escape key - ASCII
 esc       = 27,
/// Tab key - ASCII
 tab       = 9,
/// 0 - ASCII
 zero      = 48,
/// 9 - ASCII
 nine      = 57,
/// Space key - ASCII
 space = 32,



// ---------------------------- DEFAULT CURSES KEYS --------------------------- 

/// if get_wch() gives a key code
 code_yes  = 0x100,
/// not on pc kbd
 break_key     = 0x101,
/// down arrow key
 down      = 0x102,
/// up arrow key
 up        = 0x103,
/// left arrow key
 left      = 0x104,
/// right arrow key
 right     = 0x105,
/// home key
 home      = 0x106,
/// not on pc
 backspace = 0x107,
/// function keys, 64,reserved
 f0        = 0x108,

/// delete line
 dl        = 0x148,
/// insert line
 il        = 0x149,
/// delete character
 dc        = 0x14a,
/// insert char or enter ins mode
 ic        = 0x14b,
/// exit insert char mode
 eic       = 0x14c,
/// clear screen
 clear     = 0x14d,
/// clear to end of screen
 eos       = 0x14e,
/// clear to end of line
 eol       = 0x14f,
/// scroll 1 line forward
 sf        = 0x150,
/// scroll 1 line back (reverse)
 sr        = 0x151,
/// next page
 npage     = 0x152,
/// previous page
 ppage     = 0x153,
/// set tab
 stab      = 0x154,
/// clear tab
 ctab      = 0x155,
/// clear all tabs
 catab     = 0x156,
/// enter or send (unreliable)
 enter     = 0x157,
/// soft/reset (partial/unreliable)
 sreset    = 0x158,
/// reset/hard reset (unreliable)
 reset     = 0x159,
/// print/copy
 print     = 0x15a,
/// home down/bottom (lower left)
 ll        = 0x15b,
/// abort/terminate key (any)
 abort     = 0x15c,
/// short help
 shelp     = 0x15d,
/// long help
 lhelp     = 0x15e,
/// back tab key
 btab      = 0x15f,
/// beg(inning) key
 beg       = 0x160,
/// cancel key
 cancel    = 0x161,
/// close key
 close     = 0x162,
/// cmd (command) key
 command   = 0x163,
/// copy key
 copy      = 0x164,
/// create key
 create    = 0x165,
/// end key
 end       = 0x166,
/// exit key
 exit      = 0x167,
/// find key
 find      = 0x168,
/// help key
 help      = 0x169,
/// mark key
 mark      = 0x16a,
/// message key
 message   = 0x16b,
/// move key
 move      = 0x16c,
/// next object key
 next      = 0x16d,
/// open key
 open      = 0x16e,
/// options key
 options   = 0x16f,
/// previous object key
 previous  = 0x170,
/// redo key
 redo      = 0x171,
/// ref(erence) key
 reference = 0x172,
/// refresh key
 refresh   = 0x173,
/// replace key
 replace   = 0x174,
/// restart key
 restart   = 0x175,
/// resume key
 resume    = 0x176,
/// save key
 save      = 0x177,
/// shifted beginning key
 sbeg      = 0x178,
/// shifted cancel key
 scancel   = 0x179,
/// shifted command key
 scommand  = 0x17a,
/// shifted copy key
 scopy     = 0x17b,
/// shifted create key
 screate   = 0x17c,
/// shifted delete char key
 sdc       = 0x17d,
/// shifted delete line key
 sdl       = 0x17e,
/// select key
 select    = 0x17f,
/// shifted end key
 send      = 0x180,
/// shifted clear line key
 seol      = 0x181,
/// shifted exit key
 sexit     = 0x182,
/// shifted find key
 sfind     = 0x183,
/// shifted home key
 shome     = 0x184,
/// shifted input key
 sic       = 0x185,

/// shifted left arrow key
 sleft     = 0x187,
/// shifted message key
 smessage  = 0x188,
/// shifted move key
 smove     = 0x189,
/// shifted next key
 snext     = 0x18a,
/// shifted options key
 soptions  = 0x18b,
/// shifted prev key
 sprevious = 0x18c,
/// shifted print key
 sprint    = 0x18d,
/// shifted redo key
 sredo     = 0x18e,
/// shifted replace key
 sreplace  = 0x18f,
/// shifted right arrow
 sright    = 0x190,
/// shifted resume key
 srsume    = 0x191,
/// shifted save key
 ssave     = 0x192,
/// shifted suspend key
 ssuspend  = 0x193,
/// shifted undo key
 sundo     = 0x194,
/// suspend key
 suspend   = 0x195,
/// undo key
 undo      = 0x196,

/// control-left-arrow
 ctl_left  = 0x1bb,
 ctl_right = 0x1bc,
 ctl_pgup  = 0x1bd,
 ctl_pgdn  = 0x1be,
 ctl_home  = 0x1bf,
 ctl_end   = 0x1c0,

/// upper left on virtual keypad
 a1 = 0x1c1,
/// upper middle on virt. keypad
 a2 = 0x1c2,
/// upper right on vir. keypad
 a3 = 0x1c3,
/// middle left on virt. keypad
 b1 = 0x1c4,
/// center on virt. keypad
 b2 = 0x1c5,
/// middle right on vir. keypad
 b3 = 0x1c6,
/// lower left on virt. keypad
 c1 = 0x1c7,
/// lower middle on virt. keypad
 c2 = 0x1c8,
/// lower right on vir. keypad
 c3 = 0x1c9,

/// slash on keypad
 padslash      = 0x1ca,
/// enter on keypad
 padenter      = 0x1cb,
/// ctl-enter on keypad
 ctl_padenter  = 0x1cc,
/// alt-enter on keypad
 alt_padenter  = 0x1cd,
/// stop on keypad
 padstop       = 0x1ce,
/// star on keypad
 padstar       = 0x1cf,
/// minus on keypad
 padminus      = 0x1d0,
/// plus on keypad
 padplus       = 0x1d1,
/// ctl-stop on keypad
 ctl_padstop   = 0x1d2,
/// ctl-enter on keypad
 ctl_padcenter = 0x1d3,
/// ctl-plus on keypad
 ctl_padplus   = 0x1d4,
/// ctl-minus on keypad
 ctl_padminus  = 0x1d5,
/// ctl-slash on keypad
 ctl_padslash  = 0x1d6,
/// ctl-star on keypad
 ctl_padstar   = 0x1d7,
/// alt-plus on keypad
 alt_padplus   = 0x1d8,
/// alt-minus on keypad
 alt_padminus  = 0x1d9,
/// alt-slash on keypad
 alt_padslash  = 0x1da,
/// alt-star on keypad
 alt_padstar   = 0x1db,
/// alt-stop on keypad
 alt_padstop   = 0x1dc,
/// ctl-insert
 ctl_ins       = 0x1dd,
/// alt-delete
 alt_del       = 0x1de,
/// alt-insert
 alt_ins       = 0x1df,
/// ctl-up arrow
 ctl_up        = 0x1e0,
/// ctl-down arrow
 ctl_down      = 0x1e1,
/// ctl-tab
 ctl_tab       = 0x1e2,
 alt_tab       = 0x1e3,
 alt_minus     = 0x1e4,
 alt_equal     = 0x1e5,
 alt_home      = 0x1e6,
 alt_pgup      = 0x1e7,
 alt_pgdn      = 0x1e8,
 alt_end       = 0x1e9,
/// alt-up arrow
 alt_up        = 0x1ea,
/// alt-down arrow
 alt_down      = 0x1eb,
/// alt-right arrow
 alt_right     = 0x1ec,
/// alt-left arrow
 alt_left      = 0x1ed,
/// alt-enter
 alt_enter     = 0x1ee,
/// alt-escape
 alt_esc       = 0x1ef,
/// alt-back quote
 alt_bquote    = 0x1f0,
/// alt-left bracket
 alt_lbracket  = 0x1f1,
/// alt-right bracket
 alt_rbracket  = 0x1f2,
/// alt-semi-colon
 alt_semicolon = 0x1f3,
/// alt-forward quote
 alt_fquote    = 0x1f4,
/// alt-comma
 alt_comma     = 0x1f5,
/// alt-stop
 alt_stop      = 0x1f6,
/// alt-forward slash
 alt_fslash    = 0x1f7,
/// alt-backspace
 alt_bksp      = 0x1f8,
/// ctl-backspace
 ctl_bksp      = 0x1f9,
/// keypad 0
 pad0          = 0x1fa,

/// ctl-keypad 0
 ctl_pad0 = 0x1fb,
 ctl_pad1 = 0x1fc,
 ctl_pad2 = 0x1fd,
 ctl_pad3 = 0x1fe,
 ctl_pad4 = 0x1ff,
 ctl_pad5 = 0x200,
 ctl_pad6 = 0x201,
 ctl_pad7 = 0x202,
 ctl_pad8 = 0x203,
 ctl_pad9 = 0x204,

/// alt-keypad 0
 alt_pad0 = 0x205,
 alt_pad1 = 0x206,
 alt_pad2 = 0x207,
 alt_pad3 = 0x208,
 alt_pad4 = 0x209,
 alt_pad5 = 0x20a,
 alt_pad6 = 0x20b,
 alt_pad7 = 0x20c,
 alt_pad8 = 0x20d,
 alt_pad9 = 0x20e,

/// clt-delete
 ctl_del    = 0x20f,
/// alt-back slash
 alt_bslash = 0x210,
/// ctl-enter
 ctl_enter  = 0x211,

/// shift-enter on keypad
 shf_padenter = 0x212,
/// shift-slash on keypad
 shf_padslash = 0x213,
/// shift-star  on keypad
 shf_padstar  = 0x214,
/// shift-plus  on keypad
 shf_padplus  = 0x215,
/// shift-minus on keypad
 shf_padminus = 0x216,
/// shift-up on keypad
 shf_up       = 0x217,
/// shift-down on keypad
 shf_down     = 0x218,
/// shift-insert on keypad
 shf_ic       = 0x219,
/// shift-delete on keypad
 shf_dc       = 0x21a,

/// "mouse" key
 mouse     = 0x21b,
/// left-shift
 shift_l   = 0x21c,
/// right-shift
 shift_r   = 0x21d,
/// left-control
 control_l = 0x21e,
/// right-control
 control_r = 0x21f,
/// left-alt
 alt_l     = 0x220,
/// right-alt
 alt_r     = 0x221,
/// window resize
 resize    = 0x222,
/// shifted up arrow
 sup       = 0x223,
/// shifted down arrow
 sdown     = 0x224,

/// minimum curses key value
 min = break_key,
/// maximum curses key
 max = sdown
 }; // KEY

/** @} */ // Keys


/// Get the value of an fn key
/**
 * Will throw a compile time error if 12 < n < 0
 */
template<char_c n>
constexpr KEY fn() {
    static_assert(n < 12 && n > 0, "[ERROR]: awcurses::keys::f<>() called with an n value which does not satisfy 12 > n > 0");
    return static_cast<KEY>(f0 + n);
}

/// Checks if an ASCII encoded integer is an ASCII encoded number (0 to 9)
/**
 * @param ascii_char: Single ASCII encoded integer to check
 * @return true if it is a number from 0 to 9 in ASCII format, false if not
 */
constexpr bool is_number(int ascii_char) {
    // Between zero and nine, its a number
    return (ascii_char >= zero && ascii_char <= nine);
}

} // keys
} // awcurses

#endif //AWCURSES_KEYS_INL_05E3204E9B2242BB88D637BFC589C61F
