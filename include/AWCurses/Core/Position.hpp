/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
---------------------------------------
Created by Ash on 06/03/2020
Copyright (c) Natash England-Elbro 2020
----------------------------------------
/*/

#ifndef MESON_POSITION_HPP_64672576E9E14DAFB947362DAD85204B
#define MESON_POSITION_HPP_64672576E9E14DAFB947362DAD85204B

#include <AWCurses/Core/Vector2D.hpp>
/**
 * @file The header file for the Position class in AWCurses
 */

namespace awcurses {
namespace core {

/**
 * @class Position
 * @brief Stores information about the position something
 * @details Compound base. Inherited by classes which store information about where they are in some way
 */
class Position final: public Vector2D {
public:
    /// Constructor taking x and y coordiantes 
    /**
     * @param x: The x coordinate to be stored in the Position object
     * @param y: The y coordinate to be stored in the Position object
     */
    Position(const int x = 0, const int y = 0) :  Vector2D(x, y){}

     
    /// The x coordinate stored by the Position obeject
    int x() const { return _first; } 
    /// The y coordinate stored by the Position object
    int y() const { return _second; }

    /// Set the x coordinate
    void x(int x) { _first = x; }

    /// Set the y coordinate 
    void y(int y) { _second = y; }
};

} // core
} // awcurses




#endif //MESON_POSITION_HPP_64672576E9E14DAFB947362DAD85204B
