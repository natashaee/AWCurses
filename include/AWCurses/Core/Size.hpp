/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by Ash on 06/03/2020.
Copyright (c) 2020 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_CORE_SIZE_H
#define AWCURSES_CORE_SIZE_H

#include <curses.h>
#include <AWCurses/Core/Vector2D.hpp>

/**
 * @file Header file for the AWCurses Size class
 */

namespace awcurses{
namespace core {
    
/// A type for size in AWCurses
class Size final: public Vector2D {
public:
    /// Constructor which sets up the Size with height and width attributes
    /**
     * @param width: The width as an integer
     * @param height: The height as an integer
     */
    Size(const int width = COLS, const int height = LINES) : Vector2D(width, height) {}

    /// The height stored in the Size object
    int height() const { return _first; }
    /// The width stored in the Size object
    int width() const { return _second; }


    /// Set the height
    void height(int height) { _first = height; }

    /// Set the width
    void width(int width) { _second = width; }

};

} // namespace core 
} // namespace awcurses

#endif // AWCURSES_CORE_SIZE_H