/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 16/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Menu/MenuItem.hpp>
#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Menu/Menu.hpp>
#include <curses.h>

#include <utility>
#include <iostream>
#include <AWCurses/Core/Exceptions.hpp>

using namespace awcurses::core;
using namespace awcurses::menu;
using namespace std;


MenuItem::MenuItem(Menu *menu, const core::Position& position, const core::Size& size, bool selectable) : MenuItem(menu, "", "", position, size,
                                                                                                selectable) {}


MenuItem::MenuItem(Menu *menu, string title, string description, const core::Position& position, const core::Size& size, bool
selectable) : Widget(position, size), title(std::move(title)),
              description(std::move(description)), menu(menu) {
    // Move the cursor in and down
    _screen.cursor().moveX(4);
    _screen.cursor().position().y((size.height() / 2));
    // Lock it there as base position
    _screen.cursor().lockPosition();
}


void MenuItem::select() {
    // Ensure screen is setup 
    if (!_screen) _screen.show();
    
    _screen.setColourSelected();
    if (menu->using_border) {
        _screen.setLineBorder();
    }
    show();
}

void MenuItem::unselect() {
    // Unselects the menu
    
    _screen.setColourDefault();
    if (menu->using_border) {
        _screen.clearBorder();
    }
    show();
}


void MenuItem::triggerCallback() {
    if (callback) {
        callback();
    }
    
    if (selectable) {
        // If its selectable, update it and itself
        selected = true;
        show();
    }
}


void MenuItem::show() {
    if (!_screen) _screen.show();
    
    auto &c = _screen.cursor();
    try {
        _screen.clear();
        c.resetPos();
        // Print the title and description
        c << title;
        c << description;
        _screen.refresh();
    }
    catch (exceptions::InvalidScreenError &err) {
        // Catch and rethrow
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::menu::MenuItem::show()->"));
    }
    
}

void MenuItem::hide() {
    // Hides the window
    try {
        unselect();
        _screen.hide();
    }
    catch (exceptions::InvalidScreenError &err) {
        // Catch and rethrow
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::menu::MenuItem::hide()->"));
    }
}


void MenuItem::redraw() {
    _screen.refresh();
}





