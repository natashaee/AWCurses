/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by Ash on 11/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <curses.h>
#include <AWCurses/Core/KEYS.inl>
#include <AWCurses/Widgets/Layouts/ListLayout.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Widgets/Layouts/BasicLayoutItem.hpp>

using namespace awcurses::core;
using namespace awcurses::widgets::layouts;

void awcurses::widgets::layouts::ListLayout::initCursor() {
    // Super class call to init it there
    BasicLayout::initCursor();
    // Add the up item keybinding
    // Lock the Cursor
    auto &c = _screen.cursor();
    c.addKeybind(keys::up, Cursor::keybind_callback{
            [this](int) -> bool {
                upItem();
                return false;
            }});
    // Add the down item keybinding
    c.addKeybind(keys::down, Cursor::keybind_callback{
            [this](int) -> bool {
                downItem();
                return false;
            }});
}

void awcurses::widgets::layouts::ListLayout::upItem() {
    if (items.empty()) return;
    // Items to work with
    
    if (current_index <= 0) return;
    // Not the last item in the layout
    
    // Move and decrease index
    items[current_index]->unselect();
    --current_index;
    // Select the new item
    items[current_index]->select();
    
}

void awcurses::widgets::layouts::ListLayout::downItem() {
    if (items.empty()) return;
    // Items to work with
    if (items.size() < current_index + 2) return;
    // Not the last item
    
    // Move down and increase the index
    items[current_index]->unselect();
    ++current_index;
    // Select the new item
    items[current_index]->select();
}

void awcurses::widgets::layouts::ListLayout::addItem(std::shared_ptr <GenericBasicLayoutItem> item) {
    // Set starting values
    item->screen().position(next_position);
    item->screen().size(item_size);
    // Increment the values
    next_position.y(item_size.height() + next_position.y());
    
    BasicLayout::addItem(std::move(item));
}


std::weak_ptr <awcurses::widgets::layouts::GenericBasicLayoutItem>
awcurses::widgets::layouts::ListLayout::itemAt(int index) const {
    if (index + 1 > items.size()) {
        // Throw since out of range of the items
        const std::string err = "Index out of range. Thrown in: "
                                "awcurses::widgets::layouts::ListLayout::itemAt(int)";
        throw std::out_of_range(core::exceptions::err_prefix + err);
    }
    return items.at(index);
}

std::weak_ptr <GenericBasicLayoutItem> awcurses::widgets::layouts::ListLayout::operator[](int index) const {
    return itemAt(index);
}

void ListLayout::select() {
    BasicLayout::select();
    items[current_index]->select();
    int c;
    do {
        c = items[current_index]->screen().cursor().getchar(false);
        switch (c) {
            case keys::up: upItem();
                break;
            case keys::down: downItem();
                break;
            default: break;
        }
        
    } while (c);
}




