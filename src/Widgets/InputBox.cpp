/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by Ash on 09/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Core/KEYS.inl>
#include <AWCurses/Widgets/InputBox.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Screen.hpp>

using namespace std;

void awcurses::widgets::InputBox::show() {
    if (!_screen) _screen.show();
    
    // Custom logic, set the border and such
    _screen.setLineBorder();
    std::function<bool(int)> func = [this](int) {
        unselect();
        return true;
    };
    _screen.cursor().addKeybind('\t', func);
    
    // Super call
    //core::callbacks::CursorCallback(func)
    Widget::show();
    
}

void awcurses::widgets::InputBox::select() {
    // Ensure the Screen is setup
    if (!_screen.is_setup()) show();
    
    // Set its background colour to selected
    _screen.setColourSelected();
    // Get a shared_ptr lock on the Screen's Cursor
    auto &c = _screen.cursor();
    // Ensure echo is on for the Cursor
    c.echo(true);
    // Start taking input from the user
    c >> contents;
}

void awcurses::widgets::InputBox::unselect() {
    // Remove the border
    curs_set(0);
    // Reset back to default colouring
    //screen->setColourDefault();
    _screen.refresh();
    //*(getCursor()) << contents.str();
}
