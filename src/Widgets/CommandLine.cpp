/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 30/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Widgets/CommandLine.hpp>

#include <regex>
#include <utility>
#include <sstream>
#include <AWCurses/Core/Exceptions.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Screen.hpp>

using namespace awcurses::core;
using namespace awcurses::widgets;
using namespace std;


CommandLine::CommandLine(Screen *screen, std::string startup_msg) : Widget(screen, screen->position(), screen->size()), startup_msg(std::move(startup_msg)) {
    addDefaultCommands();
}

awcurses::widgets::CommandLine::CommandLine(std::string startup_msg) : CommandLine(new Screen(),
                                                                                   std::move(startup_msg)) {}


void awcurses::widgets::CommandLine::start() {
    if (!_screen) _screen.show();
    // Print the startup message
    printStartupMsg();
    // Lock the cursor here
    auto &curse = _screen.cursor();
    curse.resetX();
    curse.lockX();
    stringstream data;
    // Loop for input
    for (;;) {
        if (_commands.empty()) {
            throw core::exceptions::LogicError("AWCurses CommandLine has an empty set of commands!");
        }
        // Reset buffer
        stringstream temp;
        data.swap(temp);
        // Print the 'command waiting' prompter
        curse.moveY(1);
        curse.resetX();
        curse << "> ";
        curse >> data;
        // Iterate through and check the commands
        bool found = false;
        
        for (auto &callback : _commands) {
            auto data_str = data.str();
            if (callback.command == data_str) {
                // If its the command
                // Run the callback
                const std::list<std::string> cmds{
                    std::istream_iterator<std::string>(data), {}
                };
                // TODO: Deal with a non-zero return code here
                callback(cmds);
                
                // Set found to true so we dont error
                found = true;
                curse.resetPos();
                break;
            }
        }
        if (!found) {
            curse.resetX();
            // If we dont find a command, print the no command message
            curse.print("Command not found");
            continue;
        }
        
    }
    
}

void awcurses::widgets::CommandLine::printStartupMsg() {
    // Print out the startup message to the screen
    _screen.cursor().print(startup_msg, 0, 0, true);
}


void CommandLine::addCommand(const command_callback& callback) {
    _commands.emplace_back(callback);
}


void awcurses::widgets::CommandLine::addDefaultCommands() {
    // Add the default commands
    // Clear command
    auto clear_cmd = command_callback{
            [this](const std::list<std::string>&) -> int {
                auto &curse = _screen.cursor();
                curse.eraseAll();
                curse.position((Position()));
                printStartupMsg();
                curse.resetX();
                curse.lockPosition();
                return 0;
            }, "clear", "Clear the terminal screen"
    };
    addCommand(clear_cmd);
    auto exit_ = command_callback{
            [](const std::list<std::string>&) {
                exit(0);
                return 0;
            }, "exit", "Exit from the terminal"
    };
    // Exit command
    addCommand(exit_);
    
    // Help command
    auto help = command_callback{
            [this](const std::list<std::string> &) {
                auto &curse = _screen.cursor();
                curse.resetX();
                printHelp();
                curse.lockPosition();
                return 0;
            }, "help", "Display the built-in help message"
    };
    addCommand(help);
}

void awcurses::widgets::CommandLine::printHelp() {
    // Lock the Cursor
    auto &curse = _screen.cursor();
    curse.println("Available commands: ");
    for (const auto &cmd : _commands) {
        curse.moveY(1);
        curse.print(cmd.command + ": " + cmd.help_txt, 0, 0, true);
        curse.resetX();
    }
}

awcurses::widgets::CommandLine::~CommandLine() = default;


void awcurses::widgets::CommandLine::print(const std::string &msg, int move_lines) {
    // Lock in the Cursor and get a shared_ptr to it
    auto &curse = _screen.cursor();
    curse.resetX();
    if (!move_lines) {
        // If the lines to move is = 0; < 0 is undefined behaviour so throw an error
        // Dont jump back
        curse.print(msg, 0, 0, true);
    } else {
        // If it is moving at least a line we need to reset the cursors position first and after
        curse.moveY(1);
        curse.print(msg, 0, move_lines, true);
        curse.resetX();
    }
    curse.resetX();
    curse.lockPosition();
}

const std::vector <awcurses::widgets::CommandLine::command_callback> &CommandLine::commands() {
    return _commands;
}


CommandLine::CommandCallback::CommandCallback(func_pattern func,
                                              std::string command, std::string help_txt, bool use_regex) : ExeCallback(
        std::move(func)), command(std::move(command)), help_txt(std::move(help_txt)), use_regex(use_regex) {
}
