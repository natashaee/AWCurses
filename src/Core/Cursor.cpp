/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 05/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Core/KEYS.inl>
#include <cctype>
#include <stdexcept>
#include <cstring>
#include <sstream>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/Exceptions.hpp>


using namespace std;
using namespace awcurses;
using namespace awcurses::core;



Cursor::Cursor(Screen *screen, const Position& position) : _position(position), _zero_pos(position), _parent(screen) {}


void Cursor::set_parent(Screen *set) {
    _parent = set;
}


size_t core::Cursor::getInput(char *buffer, const size_t max_len, const char mask) {
    stringstream ss;
    try {
        int read = getInput(ss, max_len, mask);
        // Read it into the char buffer
        ss.read(buffer, read);
        return read;
    }
    catch (exceptions::InvalidScreenError &err) {
        // Rethrow with path information added
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::core::Cursor::getInput(char*)->"));
    }
}

size_t Cursor::getInput(std::stringstream &buffer, size_t max_len, char mask) {
    if (!_parent) {
        // Ensures screen is set
        throw exceptions::Exception("Cursor object has a dangling pointer reference for it's Screen! "
                                    "(awcurses::core::Cursor::getInput(stringstream))");
    }
    WINDOW *win = _parent->curses_window();
    if (!win) {
        // Checks theres a valid window before proceeding
        throw exceptions::InvalidScreenError("awcurses::core::Cursor::getInput(std::stringstream)");
    }
    
    noecho();
    // Turn on keypad
    keypad(win, true);
    wrefresh(win);
    int num = 0;
    int c;
    do {
        c = mvwgetch(win, _position.y(), _position.x());
        if (_using_custom_bindings) {
            // If custom bindings are in use and we need to check them first
            auto binding = _custom_bindings.find(c);
            if (binding != _custom_bindings.end()) {
                // If its found the custom binding
                // Execute the callback and continue
                if (binding->second(c)) {
                    // If the binding returns true, which means to breakout
                    goto end_input;
                }
                //c = mvwgetch(win, y, x);
                continue;
            }
            // Continue to the hardcoded checks
        }
        
        if (isalpha(c) || keys::is_number(c)) {
            //If its a letter not a command
            if (num > max_len) {
                // Beep at the user to let them know its too long
                beep();
                // Dont do anything with the input
                // c = mvwgetch(win, y, x);
                continue;
            }
            // Add it to the buffer
            buffer << char(c);
            if (_echo) {
                // Echoing characters on type
                if (mask == ' ') {
                    // If its echoing (not set to echo as may be end of line)
                    print(string(1, char(c)));
                } else {
                    print(string(1, mask));
                }
            }
            ++num;
            ++moved_total;
        } else {
            if (c == '\n') {
                _position.y(_position.y()+1);
                goto end_input;
                
            } else if (c == keys::space) {
                if (num > max_len - 1) {
                    // Beep at the user to let them know its too long
                    beep();
                    // Dont do anything with the input
                } else {
                    // If we can add to the buffer safely
                    // Appends the space byte
                    print(char(c));
                    print(string(1, ' '));
                    ++num;
                }
            } else if (c == keys::backspace) {
                // Backspace key
                if (num > 0) {
                    // Only erase if there are characters left
                    erase(1);
                    
                    // Rollback the buffer
                    string ch(buffer.str());
                    ch.pop_back();
                    buffer.str(ch);
                    buffer.seekp(0, ios_base::end);
                    
                    --num;
                }
                
            } else {
                // Beep at the user, we dont know what character this is!
                beep();
                continue;
            }
        }
        
    } while (c);
    return num;
    
    // Jumped too by clean input ending checks in the above loop
    end_input:
    ++num;
    // Stop the loop
    return num;
}

void Cursor::captureInput() {
    if (!_parent) {
        // Ensures screen is set
        throw exceptions::Exception("Cursor object has a dangling pointer reference for it's Screen! "
                                    "(awcurses::core::Cursor::captureInput())");
    }
    WINDOW *win = _parent->curses_window();
    if (!win) {
        // Checks theres a valid window before proceeding
        throw exceptions::InvalidScreenError("awcurses::core::Cursor::captureInput()");
    }
    // Setup curses with noecho and keypad
    noecho();
    keypad(win, true);
    
    int c;
    do {
        c = mvwgetch(win, _position.y(), _position.x());
        
        auto binding = _custom_bindings.find(c);
        if (binding != _custom_bindings.end()) {
            if (binding->second(c)) {
                // If it responds with a break command
                break;
            }
        } else if (c == keys::break_key) {
            // Break key, stop
            break;
        } else {
            // Beep at the user to let them know its an unknown keypress
            beep();
        }
    } while (c);
}

void core::Cursor::println(const string &msg, bool search_newlines) {
    // Call print but move one down
    print(msg, 0, 1, search_newlines);
    // Try and newline, if it cant just reset to its x
    try {
        moveX(-1 * (msg.size()));
    }
    catch (exceptions::OutOfBoundsException &err) {
        resetX();
    }
}


void core::Cursor::print(const string &msg, int move_x, int move_y, bool search_newlines) {
    if (!_parent) {
        // If the screen isnt valid
        throw exceptions::Exception("Cursor's Screen object is a dangling pointer! (awcurses::core::Cursor::print())");
    }
    
    if (search_newlines) {
        for (const auto &char_ : msg) {
            if (char_ == '\n') {
                // If its a newline character
                // Increase the amount of move y
                ++move_y;
            }
        }
    }
    try {
        WINDOW *win = _parent->curses_window();
        mvwprintw(win, _position.y(), _position.x(), msg.c_str());
    }
    catch (exceptions::InvalidScreenError &err) {
        // Screen has not setup
        const string path = "->awcurses::core::Cursor::print()";
        throw exceptions::InvalidScreenError(err.what() + path);
    }
    
    int msg_len = msg.length();
    moveX(msg_len);
    moveY(move_y);
}

void Cursor::print(char msg) {
    print(string(1, msg));
}


void core::Cursor::resetPos() {
    // Reset the position
    _position = _zero_pos;
}


void core::Cursor::operator<<(const std::string &msg) {
    try {
        print(msg);
    }
    catch (exceptions::InvalidScreenError &err) {
        // Rethrow with pathing
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::core::Cursor::operator<<()->"));
    }
}

int core::Cursor::operator>>(std::stringstream &buffer) {
    try {
        int size = getInput(buffer);
        return size;
    }
    catch (exceptions::InvalidScreenError &err) {
        // Rethrow with pathing prepended
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::core::Cursor::operator>>()->"));
    }
}

void core::Cursor::lockPosition() {
    // Sets its zero coordinates to its current coordinates
    lockX();
    lockY();
}

void core::Cursor::erase(int num) {
    WINDOW *win = _parent->curses_window();
    if (!win) {
        // Throw an error if it doesn't have a usable window
        throw exceptions::InvalidScreenError("awcurses::core::Cursor::erase()");
    }
    // Else, we can continue
    // Move back to the character to delete
    int done = 0;
    while (done < num) {
        if (_position.x() == 0 || _position.x() == _zero_pos.x()) {
            // If x is already 0, so last character on the line, or its the minimum it can be
            if (_position.y() != 0 && _position.y() != _zero_pos.y()) { _position.y(_position.y()-1); }
            else {
                // If its the minimum we have to break
                break;
            }
        } else {
            // If its not, just moving back
            _position.x(_position.x()-1);
        }
        
        // Delete the character
        mvwdelch(win, _position.y(), _position.x());
        ++done;
    }
}

void core::Cursor::eraseAll() {
    // Erase as many characters as have been entered
    _parent->clear();
    _parent->refresh();
}

void core::Cursor::moveX(int move) {
    int new_x = _position.x() + move;
    if (new_x < 0) {
        throw exceptions::OutOfBoundsException("Cursor was asked to moved off the screen! (y < 0) In: "
                                               "awcurses::core::Cursor::moveX()");
    } else {
        _position.x(new_x);
        moved_total += move;
    }
}

void core::Cursor::moveY(int move) {
    int new_y = _position.y() + move;
    if (new_y < 0) {
        throw exceptions::OutOfBoundsException("Cursor was asked to moved off the screen! (x < 0) In: "
                                               "awcurses::core::Cursor::moveY()");
    } else {
        _position.y(new_y);
        moved_total += move;
    }
    
}

void core::Cursor::lockX() {
    _zero_pos.x(_position.x());
    moved_total = 0;
}

void core::Cursor::lockY() {
    _zero_pos.y(_position.y());
    moved_total = 0;
}

void core::Cursor::resetX() {
    moved_total += (_zero_pos.x() - _position.x());
    _position.x(_zero_pos.x());
}

void core::Cursor::resetY() {
    moved_total += (_zero_pos.y() - _position.y());
    _position.y(_zero_pos.y());
}

void Cursor::addKeybind(int key, keybind_callback callback) {
    // Set custom bindings to true so its actually evaluated
    _using_custom_bindings = true;
    // Add the callback to the list (map) of custom bindings
    _custom_bindings.emplace(key, move(callback));
}

keys::char_c Cursor::getchar(bool echo_) {
    try {
        auto win = _parent->curses_window();
        // setup win
        keypad(win, true);
        noecho();
        
        keys::char_c c = wgetch(win);
        if (echo_) {
            // Echo the input character to the screen
            print(string(1, char(c)));
        }
        if (_using_custom_bindings) {
            auto itr = _custom_bindings.find(c);
            if (itr != _custom_bindings.end()) {
                itr->second(c);
            }
        }
        return c;
    }
    catch (exceptions::InvalidScreenError &err) {
        // Catch and rethrow
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::core::Curses::getchar(bool)->"));
    }
    
}


keys::char_c Cursor::getchar() {
    try {
        int c = getchar(_echo);
        return c;
    }
    catch (exceptions::InvalidScreenError &err) {
        // Catch and rethrow
        throw exceptions::InvalidScreenError(err.prependMsg("awcurses::core::Cursor::getchar()->"));
    }
}

const Screen *Cursor::parent() const {
    return _parent;
}


Cursor &Cursor::operator=(Cursor &&c) noexcept = default;


Cursor::Cursor(Cursor &&c) noexcept : _parent(c._parent){}


bool Cursor::CursorCallback::operator()(int key) {
    return function(key);
}

Cursor::CursorCallback::CursorCallback(std::function<bool(int)> func, bool canceling) : callbacks::Callback<bool, int>
                                                                                                (std::move(func)),
                                                                                        canceling(canceling) {}
        