/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 05/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/


#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/Exceptions.hpp>
#include <AWCurses/Core/Terminal.hpp>


using namespace awcurses::core;
using namespace std;


std::atomic_bool Terminal::_curses_setup(false);

Terminal::Terminal() {
    ensureCursesSetup();
}


void Terminal::start() {
    // Setup and initialise curses
    setlocale(LC_CTYPE, "");
    initscr();
    // Enable colour
    start_color();
    if (!has_colors()) {
        // If the terminal doesnt support colours
        throw exceptions::RuntimeError("AWCurses. AWCurses requires a terminal which supports colour, yours does not. Sorry");
    }
    // Default selected pair
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    // Default pair
    init_pair(0, COLOR_WHITE, COLOR_WHITE);
    noecho();
    timeout(-1);
    _curses_setup = true;
}

Terminal::~Terminal() {
    // Ends the window
    end();
}

void Terminal::end() {
    endwin();
    _curses_setup = false;
}


void Terminal::ensureCursesSetup() {
    if (!_curses_setup) start();
}

Terminal& Terminal::instance() {
    static Terminal instance;
    return instance;
}

const std::vector<std::unique_ptr<Screen>> &Terminal::screens() const {
    return _screens;
}

void Terminal::addScreen(std::unique_ptr<Screen> screen) {
    _screens.emplace_back(std::move(screen));
}




