/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by ash on 01/01/2020.
Copyright (c) 2020 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_TESTS_CURSORTESTS_CPP
#define AWCURSES_TESTS_CURSORTESTS_CPP

#include <gtest/gtest.h>

#include <memory>
#include <AWCurses/Core/Exceptions.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Terminal.hpp>
#include <AWCurses/Core/Screen.hpp>

using namespace awcurses::core;
using namespace std;

namespace awcurses_tests { 
namespace core {
    /// Tests the constructors
    TEST(CursorTests, CursorTests_Constructor_Test) {
        awcurses::core::Screen screen;
        Cursor cursor(&screen);
        Cursor cursor2(&screen, Position());
    }
    /// Tests Cursor's position changing functionality
    TEST(CursorTests, CursorTests_PositionTests_Test) {
        Screen screen;
        Cursor cursor(&screen);
        cursor.position(Position(2, 2));
        cursor.lockPosition();
        cursor.resetPos();
        ASSERT_EQ(cursor.position().x(), 2);
        ASSERT_EQ(cursor.position().y(), 2);
    }
    /// Tests Cursor's printing movement
    TEST(CursorTests, CursorTests_PrintMove_Test) {
        awcurses::core::Screen screen;
        try {
            screen.show();
            Cursor cursor(&screen);
            cursor.print("TEST");
            ASSERT_EQ(cursor.position().x(), 4);
            ASSERT_EQ(cursor.position().y(), 0);
            cursor.println("TEST");
            ASSERT_EQ(cursor.position().x(), 4);
            ASSERT_EQ(cursor.position().y(), 1);
        } catch(awcurses::core::exceptions::CursesException& e) {
            std::cout << "Your terminal does not seem to support curses\n";
            SUCCEED();
        }
    }
}
}

#endif //AWCURSES_TESTS_CURSORTESTS_CPP
